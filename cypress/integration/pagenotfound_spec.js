describe('Page Not Found', function() {
  // Step 1: setup the application state
  beforeEach(function() {
    const path = Math.random().toString(36).substring(2, 15);
    cy.visit(`/${path}`);
  });

  describe('Check UnAuth Menus', () => {
    it('Check required menu items are present', () => {
      // Step 2: Take an action (Sign in)
      cy.get('.mat-toolbar > .mat-focus-indicator').click();
      // Step 3: Make an assertion (Check for sign-out text)
      cy.get('.mat-menu-content > .ng-star-inserted').contains('Login');
      cy.get('[routerlink="/support"]').contains('Support');
      cy.get('[routerlink="/about"]').contains('About');
      cy.get('.cdk-overlay-backdrop').click();
      cy.get('body').contains('Page Not Found');      
    });
  });
