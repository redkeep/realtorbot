export const selectors = {
  // Auth component classes
  usernameInput: '#email',
  signInPasswordInput: '#password',
  signInSignInButton: '#btn-signin',
  signOutButton: '#btn-signout'
}

export const creds = {
  username: 'test@jasimmonsv.com',
  password: 'asdf1234'
}

describe('Menu Button', function() {
  // Step 1: setup the application state
  beforeEach(function() {
    cy.visit('/');
  });

  describe('Check UnAuth Menus', () => {
    it('Check required menu items are present', () => {
      // Step 2: Take an action (Sign in)
      cy.get('.mat-toolbar > .mat-focus-indicator').click();
      // Step 3: Make an assertion (Check for sign-out text)
      cy.get('.mat-menu-content > .ng-star-inserted').contains('Login');
      cy.get('[routerlink="/support"]').contains('Support');
      cy.get('[routerlink="/about"]').contains('About');
    });
  });

  describe('Check Routing of Menu', () => {
    it.skip('Check Support', () => {
      cy.get('.mat-toolbar > .mat-focus-indicator').click();
      cy.get('[routerlink="/support"]').contains('Support').click();
      cy.url().should('include', '/support');
      cy.get('.content').contains('Support');
    });
    it.skip('Check About', () => {
      cy.get('.mat-toolbar > .mat-focus-indicator').click();
      cy.get('[routerlink="/about"]').contains('About').click();
      cy.url().should('include', '/about');
      cy.get('.content').contains('About');
    });
    it('checks post-auth menu', () => {
      cy.get(selectors.usernameInput).type(creds.username);
      cy.get(selectors.signInPasswordInput).type(creds.password);
      cy.get(selectors.signInSignInButton).contains('Login').click();
      cy.url().should('include', '/dashboard');
      cy.get('h4').contains(creds.username);
      cy.get('.menu-button').click();
      cy.get('[routerlink="/dashboard"]').contains('Dashboard');
      cy.get('[routerlink="/listings"]').contains('Listings');
      cy.get('[routerlink="/teams"]').contains('Teams');
      // cy.get('[routerlink="/profile"]').contains(creds.username);  // TODO fix profile display
      cy.get('#btn-signout').contains('Sign Out');
      cy.get('[routerlink="/support"]').contains('Support');
      cy.get('[routerlink="/about"]').contains('About');
    });
  });

});
