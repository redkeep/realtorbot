export const selectors = {
  // Auth component classes
  usernameInput: '#email',
  signInPasswordInput: '#password',
  signInSignInButton: '#btn-signin',
  signOutButton: '#btn-signout'
}

export const creds = {
  username: 'test@jasimmonsv.com',
  password: 'asdf1234'
}

describe('Dashboard', function() {
  // Step 1: setup the application state
  beforeEach(function() {
    cy.visit('/login');
    // Step 2: Take an action (Sign in)
      cy.get(selectors.usernameInput).type(creds.username);
      cy.get(selectors.signInPasswordInput).type(creds.password);
      cy.get(selectors.signInSignInButton).contains('Login').click();
      // Step 3: Make an assertion (Check for sign-out text)
        cy.get('h4').contains(creds.username)
        // cy.get(selectors.signOutButton).contains('Sign Out');
  });

  describe('Display', () => {
    it('Show dashboard', () => {
      cy.get('h4').contains(creds.username);
      cy.get('.listing-card > .mat-card-header').contains('Listings');
      cy.get('.traffic-card > .mat-card-header').contains('Traffic Stats');
      cy.get('.calendar-card > .mat-card-header').contains('Calendar');
      cy.get('.teamfeed-card > .mat-card-header').contains('Team Feed');
    });
  });

});
