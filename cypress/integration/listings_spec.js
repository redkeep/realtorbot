export const selectors = {
  // Auth component classes
  usernameInput: '#email',
  signInPasswordInput: '#password',
  signInSignInButton: '#btn-signin',
  signOutButton: '#btn-signout'
}

export const creds = {
  username: 'test@jasimmonsv.com',
  password: 'asdf1234'
}

describe('Listing Landing', function() {
  // Step 1: setup the application state
  before(function() {
    cy.visit('/login');
    cy.get(selectors.usernameInput).type(creds.username);
    cy.get(selectors.signInPasswordInput).type(creds.password);
    cy.get(selectors.signInSignInButton).contains('Login').click();
    cy.url().should('include', '/dashboard');
  });

  beforeEach(function() {
    cy.get('.menu-button').click();
    cy.get('[routerlink="/listings"]').contains('Listings').click();
    cy.url().should('include', '/listings');
  });

  describe('Sign In', () => {
    it('allows a user to signin', () => {
      // Step 3: Make an assertion (Check for sign-out text)
        cy.get('h4').contains(creds.username)
        // cy.get(selectors.signOutButton).contains('Sign Out');
    });
  });

  describe('Display', () => {
    it('renders listing table headers', () => {
      cy.get('.listingTable').contains('Address');
      cy.get('.mat-sort-header > .mat-sort-header-container').contains('Address');
      cy.get('.mat-sort-header > .mat-sort-header-container').contains('Realtor');
      cy.get('.mat-sort-header > .mat-sort-header-container').contains('CheckIns');
      cy.get('.mat-sort-header > .mat-sort-header-container').contains('Comments');
      cy.get('.mat-sort-header > .mat-sort-header-container').contains('QR Code');
    });

    it('renders add listing button', () => {
      cy.get('.mat-fab').should('exist');
    });

    it('displays Slidebar', () => {
      cy.get('.mat-fab').click();
      cy.get('.mat-bottom-sheet-container').should('exist');
      cy.get('.mat-form-field > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-infix').should('exist');
    });

    it;
  });
});

describe.skip('Manage Listings', function() {
  // Step 1: setup the application state
  before(function() {
    cy.visit('/login');
    cy.get(selectors.usernameInput).type(creds.username);
    cy.get(selectors.signInPasswordInput).type(creds.password);
    cy.get(selectors.signInSignInButton).contains('Login').click();
    cy.url().should('include', '/dashboard');
  };

  beforeEach(function() {
    cy.get('.menu-button').click();
    cy.get('[routerlink="/listings"]').contains('Listings').click();
    cy.url().should('include', '/listings');
  });

  describe('Sign In', () => {
    it('allows a user to signin', () => {
      // Step 3: Make an assertion (Check for sign-out text)
        cy.get('h4').contains(creds.username)
        // cy.get(selectors.signOutButton).contains('Sign Out');
    });

    it('creates listing', () => {
      //initalize needed params
      let houseNumber = Math.floor(Math.random() * (99999 - 100) + 100);
      let street = Math.random().toString(36).substring(2, 15);
      let address = `${houseNumber} ${street}`;
      let zipcode = Math.floor(Math.random() * (99999 - 10000) + 10000);
      cy.get('.mat-fab').click();
      cy.get('.mat-bottom-sheet-container').should('exist');
      cy.get('[name="addListingAddress"]').should('exist');
      cy.get('[name="addListingAddress"]').type(address);
      cy.get('[name="addZipcode"]').should('exist');
      cy.get('[name="addZipcode"]').type(zipcode);
      cy.get('[name="submitListing"]').should('exist');
      cy.get('[name="submitListing"]').click();
      cy.get('table').contains('td', address).should('exist');
      //TODO delete listing
    });

    it.skip('archives listing', () => {

    });
  });
};
