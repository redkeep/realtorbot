export const selectors = {
  // Auth component classes
  usernameInput: '#email',
  signInPasswordInput: '#password',
  signInSignInButton: '#btn-signin',
  signOutButton: '#btn-signout'
}

export const creds = {
  username: 'test@jasimmonsv.com',
  password: 'asdf1234'
}

describe('Authenticator', function() {
  // Step 1: setup the application state
  beforeEach(function() {
    cy.visit('/login');
  });

  describe('UnAuth Routing', () => {
    it('Routes unauth dashboard to login', () => {
      cy.visit('/dashboard');
      cy.url().should('include', '/login');
      cy.get('.content').should('not.contain', 'Page Not Found');
    });

    it('Routes unauth listings to login', () => {
      cy.visit('/listings');
      cy.url().should('include', '/login');
      cy.get('.content').should('not.contain', 'Page Not Found');
    });

    it('Routes unauth team to login', () => {
      cy.visit('/teams');
      cy.url().should('include', '/login');
      cy.get('.content').should('not.contain', 'Page Not Found');
    });

    it.skip('Displays unauth about', () => {
      cy.visit('/about');
      cy.url().should('include', '/about');
      cy.get('.content').should('not.contain', 'Page Not Found');
    });

    it.skip('Displays unauth support', () => {
      cy.visit('/support');
      cy.url().should('include', '/support');
      cy.get('.content').should('not.contain', 'Page Not Found');
    });

  });

  describe('Sign In', () => {
    it('allows a user to signin', () => {
      // Step 2: Take an action (Sign in)
      cy.get(selectors.usernameInput).type(creds.username);
      cy.get(selectors.signInPasswordInput).type(creds.password);
      cy.get(selectors.signInSignInButton).contains('Login').click();
      // Step 3: Make an assertion (Check for sign-out text)
        cy.get('h4').contains(creds.username)
        // cy.get(selectors.signOutButton).contains('Sign Out');
    });
  });

  describe('Sign Out', () => {
    it('allows a user to signout', () => {
      cy.get('.menu-button').click();
      cy.get('.mat-menu-content > .ng-star-inserted').contains('Login');
      cy.get('.cdk-overlay-backdrop').click();
      cy.get(selectors.usernameInput).type(creds.username);
      cy.get(selectors.signInPasswordInput).type(creds.password);
      cy.get(selectors.signInSignInButton).contains('Login').click();
      cy.url().should('include', '/dashboard');
      cy.get('h4').contains(creds.username);
      cy.get('.menu-button').click();
      cy.get('#btn-signout').contains('Sign Out').click();
      cy.url().should('include', '/login');
    });
  });

});

describe('SignUp vs SignIn', function() {
  beforeEach(function() {
    cy.visit('/login');
  });

  describe('Switch to SignUp', () => {
    it('allows a user to switch to signup', () => {
      cy.get('#btn-signin').contains('Login');
      cy.get('#btn-switchMode').contains('Sign Up').click();
      cy.get('#btn-signin').contains('Sign Up');
      cy.get('#btn-switchMode').contains('Login').click();
      cy.get('#btn-signin').contains('Login');
    });
  });

  });  // Failure to login
}); // Switch for SignUp

describe('Wrong Login', function() {
  beforeEach(function() {
    cy.visit('/login');
  });

  describe('invalid email', () => {
    it('checks email is valid', () => {
      cy.get(selectors.usernameInput).type('invalid email address');
      cy.get(selectors.signInPasswordInput).type('nopassword');
      cy.get('#email').should('have.css', 'border', '1px solid rgb(255, 0, 0)');
      cy.get('.help-block').contains('Please enter a valid email!');
      cy.get(selectors.signInSignInButton).should('be.disabled');
    });
  });

  describe('Account NaN', () {
    it('fails on non-existing account', () => {
      cy.get(selectors.usernameInput).type('invalid@example.com');
      cy.get(selectors.signInPasswordInput).type('nopassword');
      cy.get('#btn-signin').contains('Login').click();
      cy.get('.mat-snack-bar-container').contains('An error occurred!');
    })
  }); // Account NaN

  describe('Account wrong password', () {
    it('fails to log into recorded account', () => {
      cy.get(selectors.usernameInput).type(creds.username);
      cy.get(selectors.signInPasswordInput).type('nopassword');
      cy.get('#btn-signin').contains('Login').click()
      cy.get('.mat-snack-bar-container').contains('An error occurred!');
    });

  }); // Account NaN
}); // Wrong Login
