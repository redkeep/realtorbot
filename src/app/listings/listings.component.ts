import { Component, OnInit } from '@angular/core';
import Auth from "@aws-amplify/auth";
import sha256 from 'crypto-js/sha256';
import { Sort } from '@angular/material/sort';
import { paths } from '../app-paths';
import { API, Logger } from 'aws-amplify';
import { NgForm } from '@angular/forms';
import { Router } from "@angular/router";
// import * as queries from '../../graphql/queries';
import { APIService } from '../API.service';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, } from '@angular/material/snack-bar';

export interface Listings {
  address: string;
  owner: string;
  hash: string;
  // checkins: [string];
  comments?: number;
  url?: string;
  propertyURL?: string;
}

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ["./listings.component.scss"],
})
export class ListingsComponent implements OnInit {
  error: string = null;
  success: string = null;
  warning: string = null;
  notice: string = null;
  listings: Listings[] = [];
  profile: any = {};
  username: string = null;
  selfURL: string = document.baseURI;
  elementType: 'url' | 'canvas' | 'img' = 'url';
  sortedData: Listings[] = [];

  constructor(
    private api: APIService,
    private _router: Router,
    private _bottomSheet: MatBottomSheet,
    private _snackBar: MatSnackBar){}



  sortData(sort: Sort) {
    const data = this.listings.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'address': return compare(a.address, b.address, isAsc);
        case 'owner': return compare(a.owner, b.owner, isAsc);
        // case 'checkins': return compare(a.num_checkins, b.num_checkins, isAsc);
        case 'comments': return compare(a.comments, b.comments, isAsc);
        default: return 0;
      }
    });
  }

  async ngOnInit() {
    const logger = new Logger('listings.Init');
    this.getUserInfo().then(() => {
      if (this.profile != null) {
        this.username = this.profile.attributes.email;
      } else {
        this._router.navigate(["/login"]);
      }
    })
    .catch(error => {
      logger.error(error);
    });
    this.getListings();
  } // OnInit

  async getUserInfo() {
    const logger = new Logger('listings.getUserInfo')
    this.profile = await Auth.currentUserInfo();
    logger.debug(this.profile);
  } // getUserInfo

  // build a unique listing ID that will never change
  getId ( msg: string) {
    return sha256(`${Date.now()}::${this.profile.username}::${msg}`);
  } //getId

  // build a listing hash that can be recreated if desired
  getHash ( msg: string ) {
    return sha256(`${Date.now()}::${msg}`);
  } //getHash

  getListings(){
    const logger = new Logger('listings.getListings');
    const getListings = this.api.ListListings();
    // const getListings = this.api.ListListings();
    getListings.then((evt) => {
      logger.debug(evt);
      this.listings = evt.items;
      for (let i = 0; i< this.listings.length; i++) {
        this.listings[i].url = `${document.baseURI}${paths.visit}/${this.listings[i].hash}`;
        this.listings[i].propertyURL = `${paths.property}/${this.listings[i].hash}`;
      }
      this.sortedData = this.listings.slice();
    })
    .catch((error) => {
      logger.debug(error);
      if (error.data) {
        logger.warn(error);
        const evt = error.data.listListings;
        this.listings = evt.items;
        for (let i = 0; i< this.listings.length; i++) {
          this.listings[i].url = `${document.baseURI}${paths.visit}/${this.listings[i].hash}`;
          this.listings[i].propertyURL = `${paths.property}/${this.listings[i].hash}`;
        }
        this.sortedData = this.listings.slice();
      } else {
        logger.error(error);
        this.openSnackBar('Error getting Listings.', 'error');
      }
    });
  }

  openBottomSheet(): void {
    const logger = new Logger("listings.listingsAPI")
    const bottomSheetRef = this._bottomSheet.open(BottomSheetAddListingSheet);

    bottomSheetRef.afterDismissed().subscribe((dataFromChild) => {
      logger.debug(dataFromChild);
      this.getListings();
    });
  }

  openSnackBar(msg: string, type: string = 'error') {
    let duration = 5000;
    let h_pos: MatSnackBarHorizontalPosition = 'center';
    let v_pos: MatSnackBarVerticalPosition = 'top';
    switch(type) {
      case 'error': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
      case 'success': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      case 'warn': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      default: {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
    };
    this._snackBar.open(msg, 'dismiss', {
      duration: duration,
      horizontalPosition: h_pos,
      verticalPosition: v_pos,
    });
  } // openSnackBar

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1: -1);
}

@Component({
  selector: 'bottom-sheet-add-listing',
  templateUrl: 'bottom-sheet-add-listing.html',
})
export class BottomSheetAddListingSheet {
  listings: Listings[] = [];
  profile: any = {};

  constructor(
    private _bottomSheetRef:MatBottomSheetRef<BottomSheetAddListingSheet>,
    private api: APIService,
  ) {
    this.profile
    this.getUserInfo();
  }

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

  onSubmit(form: NgForm) {
    const logger = new Logger('listings.onSubmit');
    this.createListing( form.value.addListingAddress,
                        form.value.addZipcode )
    .then((resp: any) => {
      this._bottomSheetRef.dismiss(this.listings);
    })
    .catch(error => {
      logger.error(error);
    });
  }

  getHash ( msg: string ) {
    return sha256(`${Date.now()}::${msg}`);
  } //getHash

  async getUserInfo() {
    const logger = new Logger('listings.BottomSheet.getUserInfo');
    this.profile = await Auth.currentUserInfo();
    logger.debug(this.profile);
  } // getUserInfo

  async createListing ( address: string, zipcode: string ) {
    const logger = new Logger('listings.BottomSheet.createListing');
    logger.debug(`address: ${address}`);
    logger.debug(`zipcode: ${zipcode}`);
    logger.debug(`owner: ${this.profile.username}`);
    this.api.CreateListing({
      address: address,
      hash: `${this.getHash(address)}`,
      zipcode: zipcode,
      owner: this.profile.username
    })
    .catch(error => {
      logger.error(error);
    });
  }
}
