export const paths = {
  dashboard: 'dashboard',
  login: 'login',
  listings: 'listings',
  property: 'property',
  visit: 'visit',
  teams: 'teams',
  about: 'about',
  support: 'support'
}
