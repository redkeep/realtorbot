import { Component, OnInit } from "@angular/core";
import Auth from "@aws-amplify/auth";
import { Sort } from '@angular/material/sort';
import { Logger } from 'aws-amplify';
import { Router } from "@angular/router";
import { APIService } from '../API.service';
import { MatMenuModule } from '@angular/material/menu';
import { TopBarComponent } from '../top-bar/top-bar.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, } from '@angular/material/snack-bar';

export interface CheckIns {
  email: string;
}

export interface Listings {
  address: string;
  hash: string;
  comments?: number;
  url?: string;
}

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})

export class DashboardComponent implements OnInit {
  listings: Listings[] = [];
  profile: any = {};
  username: string = null;
  sortedData: Listings[];

  constructor(
    private api: APIService,
    private _router: Router,
    private _snackBar: MatSnackBar){}

  sortData(sort: Sort) {
    const data = this.listings.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'address': return compare(a.address, b.address, isAsc);
        // case 'owner': return compare(a.owner, b.owner, isAsc);
        // case 'checkins': return compare(a.checkins, b.checkins, isAsc);
        case 'comments': return compare(a.comments, b.comments, isAsc);
        default: return 0;
      }
    });
  }

  ngOnInit() {
    const logger = new Logger('dashboard.Init');
    this.getUserInfo().then(() => {
      if (this.profile != null) {
        this.username = this.profile.attributes.email;
      } else {
        this._router.navigate(["/login"]);
      }
    })
    .catch(error => {
      logger.error(error);
      this.openSnackBar('An Error occured', 'error');
    });
    logger.debug(`Profile: ${this.profile}`); // TODO: Remove for production
    logger.debug(`Username: ${this.username}`); // TODO: Remove for production
    const getListings = this.api.ListListings();
    getListings.then((evt) => {
      this.listings = evt.items;
      this.sortedData = this.listings.slice();
    })
    .catch(error => {
      if (error.data.listListings && error.data.listListings.items.length > 0) {
        this.listings = error.data.listListings.items;
        this.sortedData = this.listings.slice();
      } else {
        logger.error(error);
      this.openSnackBar('Failed to get Listings.', 'error');
      }
    });
  }

  async getListings() {
    return this.api.ListListings();
  }

  async getUserInfo() {
    this.profile = await Auth.currentUserInfo();
  }

  async getListingComments() {

  }

  openSnackBar(msg: string, type: string = 'error') {
    let duration = 5000;
    let h_pos: MatSnackBarHorizontalPosition = 'center';
    let v_pos: MatSnackBarVerticalPosition = 'top';
    switch(type) {
      case 'error': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
      case 'success': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      case 'warn': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      default: {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
    };
    this._snackBar.open(msg, 'dismiss', {
      duration: duration,
      horizontalPosition: h_pos,
      verticalPosition: v_pos,
    });
  } // openSnackBar
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1: -1);
}
