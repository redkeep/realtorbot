//Library Imports
import { NgModule } from '@angular/core';
import { QRCodeModule } from 'angularx-qrcode';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from './material-module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// App imports
import { AppComponent } from './app.component';
import { VisitComponent } from './visit/visit.component';
import { TeamsComponent } from './teams/teams.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { AppRoutingModule } from './app-routing.module';
import { PropertyComponent } from './property/property.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './pagenotfound/pagenotfound.component';
import { UserAuthenticationComponent } from './auth/user-authentication.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListingsComponent, BottomSheetAddListingSheet } from './listings/listings.component';

@NgModule({
  imports: [
    FormsModule,
    QRCodeModule,
    BrowserModule,
    MaterialModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  declarations: [
    AppComponent,
    VisitComponent,
    TeamsComponent,
    TopBarComponent,
    PropertyComponent,
    ListingsComponent,
    DashboardComponent,
    PageNotFoundComponent,
    BottomSheetAddListingSheet,
    UserAuthenticationComponent,
  ],
  bootstrap: [AppComponent],
  providers: []
})
export class AppModule {}
