import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { APIService } from '../API.service';
import { API, Logger } from 'aws-amplify';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, } from '@angular/material/snack-bar';

export interface Listing {
  id: string;
  address: string;
  hash: string;
  comments?: number;
  url?: string;
  owner?: string;
  images?: Array<string>;
}

@Component({
  selector: 'app-visit',
  templateUrl: './visit.component.html',
  styleUrls: ['./visit.component.scss'],
})
export class VisitComponent implements OnInit {
  checkedin: Boolean = false;
  loading: Boolean = false;
  images: Array<string>;
  listing: Listing = {
    id: null,
    address: '*Loading*',
    // owner: '',
    hash: '*Loading*',
    // checkins: [''],
    images: [],
    comments: 0
  };
  propertyId: string = '';
  email: string = '';
  name: string = '';
  phone: string = '';
  owner: string = '';

  constructor(
    private api: APIService,
    private route: ActivatedRoute,
    private _router: Router,
    private _snackBar: MatSnackBar){}

  ngOnInit() {
    const logger = new Logger('visit.Init');
    this.images = [
        'https://image.shutterstock.com/image-photo/beautiful-exterior-newly-built-luxury-600w-529108441.jpg',
        'https://image.shutterstock.com/image-photo/big-custom-made-luxury-house-600w-1677499762.jpg',
        'https://charlotteagenda-charlotteagenda.netdna-ssl.com/wp-content/uploads/2020/05/real-estate-listing-front-porches.jpg'
      ];
    // get listing record hash
    const hash: string = this.route.snapshot.paramMap.get('hash');
    this.loading = true;
    // get listing
    const listingPromise = this.listingsAPI(hash);
    // process getListing
    listingPromise.then((evt) => {
      logger.debug(evt);
      // if no results, then this is not a proper record
      if (evt.items.length < 1) {
        this._router.navigate(["/404"]);
      };
      this.listing = evt.items[0];
    })
    .catch(error => {
      logger.debug(error);
      logger.debug(error.data);
      if (error.data.listListings && error.data.listListings.items.length > 0) {
        this.listing = error.data.listListings.items[0];
      } else {
        logger.error(error);
        this.openSnackBar('Error getting listing.', 'error');
        // this.router.navigate(["/404"]);
    }
    });
    this.loading = false;
  }

  checkIn (form: NgForm) {
    if (!form.valid) {
      return;
    }
    const logger = new Logger('visit.checkIn');
    logger.debug(form);
    const listingUpdatePromise = this.createCheckinAPI(
      form.value.checkinName,
      form.value.checkinPhone,
      form.value.checkinEmail,
      this.listing.id,
      this.listing.owner,
    );
    // listingUpdatePromise.then((evt) => {
    //     logger.debug(evt)
    //     logger.debug(`this.email: ${this.email}`) //TODO delete me for Prod
    // })
    this.email = form.value.checkinEmail;
    this.checkedin = true;
  }

  async createCheckinAPI(name: string,
    phone_number: string,
    email: string,
    listingID: string,
    owner: string
  ) {
    const result: any = await API.graphql({
      variables: {
        input: {
          name: name,
          phone_number: phone_number,
          email: email,
          listingID: listingID,
          owner: owner
        }
      },
      query: `mutation CreateCheckIns($input: CreateCheckInsInput!, $condition: ModelCheckInsConditionInput) {
        createCheckIns(input: $input, condition: $condition) {
          __typename
          id
          name
          email
          owner
          phone_number
          listingID
          createdAt
          updatedAt
        }
      }`,
      // @ts-ignore
      authMode: 'AWS_IAM'
    })
  }

  async listingsAPI(hashId: string){
    const result: any = await API.graphql({
      query: `query ListListings($filter: ModelListingFilterInput, $limit: Int, $nextToken: String) {
        listListings(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            hash
            address
            zipcode
            bedrooms
            full_bathroom
            half_bathroom
            owner
            checkin {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          nextToken
        }
      }`,
      variables: {filter: {hash: {eq: hashId}}},
      // @ts-ignore
      authMode: 'AWS_IAM'
      // authMode: Auth.currentAuthenticatedUser() ? 'AMAZON_COGNITO_USER_POOLS' : 'AWS_IAM'
    })
    return result;
  }

  addNotes (form: NgForm){
    // TODO add Notes
    const logger = new Logger('visit.addNotes');
    this.loading = true;
    logger.debug(form);
    const addNotesPromise = this.createNotesAPI(
      this.listing.id,
      this.email,
      form.value.notes,
      this.listing.owner
    );
    addNotesPromise.then((evt) => {
      logger.debug(evt)
      form.reset();
      this.openSnackBar('Success adding note.', 'success');
    })
    .catch (error => {
      logger.error(error);
      this.openSnackBar('Error adding note.', 'error');
    });
    this.loading = false;
  }

  async createNotesAPI(id: string, email: string, notes: string, owner: string){
    const logger = new Logger('visit.createNotesAPI');
    logger.debug(`id: ${id}`);
    logger.debug(`email: ${email}`);
    logger.debug(`notes: ${notes}`);
    this.loading = true;
    const result: any = await API.graphql({
      variables: {
        input: {
          email: email,
          listingID: id,
          content: notes,
          owner: owner
        }
      },
      query: `mutation CreateComment($input: CreateCommentInput!, $condition: ModelCommentConditionInput) {
          createComment(input: $input, condition: $condition) {
            __typename
            listingID
            email
            content
            createdAt
          }
        }`,
      // @ts-ignore
      authMode: 'AWS_IAM'
    });
    this.loading = false;
    return result;
  } // createNotesAPI

  openSnackBar(msg: string, type: string = 'error') {
    let duration = 5000;
    let h_pos: MatSnackBarHorizontalPosition = 'center';
    let v_pos: MatSnackBarVerticalPosition = 'top';
    switch(type) {
      case 'error': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
      case 'success': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      case 'warn': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      default: {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
    };
    this._snackBar.open(msg, 'dismiss', {
      duration: duration,
      horizontalPosition: h_pos,
      verticalPosition: v_pos,
    });
  } // openSnackBar

} //end component
